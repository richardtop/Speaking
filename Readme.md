# Public Talks by [Richard Topchii](https://github.com/richardtop/)

|DD/MM/YYYY|Event|Topic|Location|Downloads|
|---|---|---|---|---|
|07/05/2020|Type Theory seminar, University of Helsinki (Online event)|Swift & it's Type System.|[Helsinki, 🇫🇮](https://goo.gl/maps/A9DpFRbfsDMKpvmS6)|[PDF](https://github.com/richardtop/Speaking/files/4871915/Swift.Type.System.pdf), [Keynote](https://github.com/richardtop/Speaking/files/4871913/Swift.Type.System.key.zip)|
|10/09/2019|[CocoaHeads Stockholm - Apple Special Event @ Developers Bay](https://www.meetup.com/CocoaHeads-Stockholm/events/264363877/)|Technology and Liberal Arts|[Stockholm, 🇸🇪](https://goo.gl/maps/9Xe83A4AMN2xu4xF8)|[Keynote](https://www.dropbox.com/s/oxejl6eirxdxcys/Stockholm.key?dl=0)|
|20/07/2019|[Helsinki iOS - Autumn is coming, so it's Apple season](https://www.meetup.com/HelsinkiOS-Mac-and-iOS-Developers/events/263769988/)|Technology and Liberal Arts|[Helsinki, 🇫🇮](https://goo.gl/maps/A9DpFRbfsDMKpvmS6)|[Keynote](https://www.dropbox.com/s/lj9gpwn56jin5dm/Helsinki.key?dl=0)|
|04/06/2019|[Helsinki iOS - WWDC Special](https://www.meetup.com/HelsinkiOS-Mac-and-iOS-Developers/events/261505398/)|The Recipe of a Popular Library|[Helsinki, 🇫🇮](https://goo.gl/maps/A9DpFRbfsDMKpvmS6)|[PDF](https://github.com/richardtop/Talks/files/3272691/wwdc.pdf), [Keynote](https://github.com/richardtop/Talks/files/3272700/wwdc.key.zip)|

